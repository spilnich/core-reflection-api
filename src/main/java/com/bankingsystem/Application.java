package com.bankingsystem;

public class Application {

    public static void main(String[] args) {
        Request request = new Request(
                TransactionType.REFUND,
                1000,
                3211550184080934L,
                12,
                23,
                "Vasya",
                "Pupkin");

        Bank bank = new CustomBank();
        bank = new ProxyBank(bank);

        bank.executeTransaction(request);
    }
}
