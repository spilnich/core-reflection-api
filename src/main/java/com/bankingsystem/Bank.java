package com.bankingsystem;

public interface Bank {

    void executeTransaction(Request request);
}
