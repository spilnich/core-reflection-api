package com.bankingsystem;

import com.bankingsystem.annotations.ReceiptField;

import java.time.LocalDateTime;

public class Response {

    @ReceiptField
    private LocalDateTime time;
    @ReceiptField
    private String firstname;
    @ReceiptField
    private String lastname;
    @ReceiptField
    private Long cardNumber;
    @ReceiptField
    private Integer incomeAmount;
    @ReceiptField
    private Double fee;
    @ReceiptField
    private Double outcomeAmount;

    public void responseToString() {
        System.out.println("******************************************" + "\n" +
                "RECEIPT:" + "\n" +
                "date: " + time + "\n" +
                firstname + " " + lastname + "\n" +
                "cardNumber: " + cardNumber + "\n" +
                "incomeAmount: " + incomeAmount + "\n" +
                "fee: " + fee + "%" + "\n" +
                "outcomeAmount: " + outcomeAmount + "\n" +
                "******************************************");
    }
}
