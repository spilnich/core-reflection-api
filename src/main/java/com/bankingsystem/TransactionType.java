package com.bankingsystem;

public enum TransactionType {

    SALE,
    REFUND
}
