package com.bankingsystem;

import java.util.Arrays;

public enum BankCardType {

    PRIVAT("424400", 0.5),
    MONO("590012", 1.0),
    OSCHAD("321155", 2.0);

    private final String label;
    private final double fee;

    BankCardType(String label, double fee) {
        this.label = label;
        this.fee = fee;
    }

    public static double getBankFee(String subCardNumber) {
        return Arrays.stream(BankCardType.values())
                .filter(type -> subCardNumber.equals(type.label))
                .findFirst()
                .map(type -> type.fee)
                .orElse(0.0);
    }
}
