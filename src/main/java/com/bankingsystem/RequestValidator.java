package com.bankingsystem;

import com.bankingsystem.annotations.Fee;
import com.bankingsystem.annotations.Length;
import com.bankingsystem.annotations.Range;

import java.lang.reflect.Field;
import java.util.Arrays;

public final class RequestValidator {

    public static final String CARD_NUMBER = "cardNumber";
    public static final int CARD_NUMBER_LENGTH = 16;
    public static final int END_INDEX = 6;

    private RequestValidator() {
    }

    static boolean checkRange(Request request, int value, String fieldName) {
        return Arrays.stream(getFields(request))
                .filter(field -> field.isAnnotationPresent(Range.class) && field.getName().equals(fieldName))
                .anyMatch(field -> {
                    Range range = field.getAnnotation(Range.class);

                    return value >= range.min() && value <= range.max();
                });
    }

    static boolean checkSymbolsAcceptability(Request request, SymbolType type, String fieldName) {
        Object value = null;
        Field field = getField(request, fieldName);
        if (field != null) {
            field.setAccessible(true);
            value = getFieldValue(request, field);
        }
        return type.matches(String.valueOf(value));
    }

    static boolean checkLength(Request request) {
        return Arrays.stream(getFields(request))
                .filter(field -> field.isAnnotationPresent(Length.class) && field.getName().equals(CARD_NUMBER))
                .anyMatch(field -> {
                    Field cardNumber = getField(request, CARD_NUMBER);
                    cardNumber.setAccessible(true);
                    Object value = getFieldValue(request, cardNumber);

                    return String.valueOf(value).length() == CARD_NUMBER_LENGTH;
                });
    }

    private static <T> Field[] getFields(T object) {
        return object.getClass().getDeclaredFields();
    }

    static <T> Field getField(T object, String fieldName) {
        return Arrays.stream(getFields(object))
                .filter(field -> field.getName().equals(fieldName))
                .findFirst()
                .orElse(null);
    }

    private static boolean isFeeFieldExists(Request request) {
        return Arrays.stream(getFields(request))
                .anyMatch(field -> field.getName().equals("fee"));
    }

    static <T> Object getFieldValue(T object, Field field) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            System.out.println("getting field value exception");
        }
        return null;
    }

    public static double getFee(Request request) {
        String sub = getSubString(request);
        boolean fee = false;

        if (isFeeFieldExists(request)) {
            Field feeField = getField(request, "fee");
            if (feeField != null) {
                feeField.setAccessible(true);
                fee = feeField.getAnnotation(Fee.class).enabled();
            }
        }
        return fee ? BankCardType.getBankFee(sub) : 0.0;
    }

    private static String getSubString(Request request) {
        Object value = null;
        Field cardNumberField = getField(request, CARD_NUMBER);
        if (cardNumberField != null) {
            cardNumberField.setAccessible(true);
            value = getFieldValue(request, cardNumberField);
        }
        return String.valueOf(value).substring(0, END_INDEX);
    }
}
