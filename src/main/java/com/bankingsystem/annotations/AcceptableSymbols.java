package com.bankingsystem.annotations;

import com.bankingsystem.SymbolType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AcceptableSymbols {

    SymbolType value();
}
