package com.bankingsystem;

import java.util.regex.Pattern;

public enum SymbolType {

    ALPHABETIC(Pattern.compile("\\p{Alpha}*")),
    DIGITS(Pattern.compile("\\p{Digit}*"));

    private final Pattern pattern;

    SymbolType(Pattern pattern) {
        this.pattern = pattern;
    }

    public boolean matches(String text) {
        return pattern.matcher(text).matches();
    }
}
