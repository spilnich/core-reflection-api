package com.bankingsystem;

import com.bankingsystem.annotations.ReceiptField;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

import static com.bankingsystem.RequestValidator.*;

public class CustomBank implements Bank {

    public static final int DIVISION_NUMBER = 100;

    @Override
    public void executeTransaction(Request request) {
        try {
            Class<Response> responseClass = Response.class;
            Constructor<Response> constructor = getResponseConstructor(responseClass);

            Response response = constructor.newInstance();

            setResponseParameters(request, response);
            printResponse(response);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            System.out.println("creating response exception");
        }
    }

    private void setResponseParameters(Request request, Response response) throws IllegalAccessException {
        setResponseFirstname(response, request);
        setResponseLastname(response, request);
        setResponseCardNumber(response, request);
        setResponseFee(response, request);
        setResponseIncomeAmount(response, request);
        setResponseOutcomeAmount(response, request);
        setResponseTime(response);
    }

    private Constructor<Response> getResponseConstructor(Class<Response> responseClass) throws NoSuchMethodException {
        return responseClass.getConstructor();
    }

    private Long getRequestCardNumber(Request request) {
        Field cardNumber = getField(request, "cardNumber");
        cardNumber.setAccessible(true);
        return (Long) getFieldValue(request, cardNumber);
    }

    private void setResponseCardNumber(Response response, Request request) throws IllegalAccessException {
        Field cardNumber = getField(response, "cardNumber");
        cardNumber.setAccessible(true);
        if (checkTransactionField(cardNumber)) {
            cardNumber.set(response, getRequestCardNumber(request));
        }
    }

    private String getRequestLastname(Request request) {
        Field lastname = getField(request, "lastname");
        lastname.setAccessible(true);
        return (String) getFieldValue(request, lastname);
    }

    private void setResponseLastname(Response response, Request request) throws IllegalAccessException {
        Field lastname = getField(response, "lastname");
        lastname.setAccessible(true);
        if (checkTransactionField(lastname)) {
            lastname.set(response, getRequestLastname(request));
        }
    }

    private String getRequestFirstname(Request request) {
        Field firstname = getField(request, "firstname");
        firstname.setAccessible(true);
        return (String) getFieldValue(request, firstname);
    }

    private void setResponseFirstname(Response response, Request request) throws IllegalAccessException {
        Field firstname = getField(response, "firstname");
        firstname.setAccessible(true);
        if (checkTransactionField(firstname)) {
            firstname.set(response, getRequestFirstname(request));
        }
    }

    private void setResponseFee(Response response, Request request) throws IllegalAccessException {
        Field fee = getField(response, "fee");
        fee.setAccessible(true);
        if (checkTransactionField(fee)) {
            fee.set(response, getFee(request));
        }
    }

    private Integer getRequestIncomeAmount(Request request) {
        Field incomeAmount = getField(request, "amount");
        incomeAmount.setAccessible(true);
        return (Integer) getFieldValue(request, incomeAmount);
    }

    private void setResponseIncomeAmount(Response response, Request request) throws IllegalAccessException {
        Field incomeAmount = getField(response, "incomeAmount");
        incomeAmount.setAccessible(true);
        if (checkTransactionField(incomeAmount)) {
            incomeAmount.set(response, getRequestIncomeAmount(request));
        }
    }

    private void setResponseOutcomeAmount(Response response, Request request) throws IllegalAccessException {
        Field outcomeAmount = getField(response, "outcomeAmount");
        outcomeAmount.setAccessible(true);
        if (checkTransactionField(outcomeAmount)) {
            outcomeAmount.set(response, getOutcomeAmount(getRequestIncomeAmount(request), getFee(request)));
        }
    }

    private void setResponseTime(Response response) throws IllegalAccessException {
        Field time = getField(response, "time");
        time.setAccessible(true);
        if (checkTransactionField(time)) {
            time.set(response, LocalDateTime.now());
        }
    }

    private double getOutcomeAmount(Integer amount, Double fee) {
        return fee != 0 ? amount - (amount * fee / DIVISION_NUMBER) : amount;
    }

    private void printResponse(Response response) throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {
        Method method = response.getClass().getDeclaredMethod("responseToString");
        method.invoke(response);
    }

    private boolean checkTransactionField(Field field) {
        return field.isAnnotationPresent(ReceiptField.class);
    }
}