package com.bankingsystem;

import com.bankingsystem.annotations.AcceptableSymbols;
import com.bankingsystem.annotations.Fee;
import com.bankingsystem.annotations.Length;
import com.bankingsystem.annotations.Range;

public class Request {

    private final TransactionType type;
    @Range(min = 1, max = 10_000)
    @AcceptableSymbols(SymbolType.DIGITS)
    private final int amount;
    @Length()
    @AcceptableSymbols(SymbolType.DIGITS)
    private final long cardNumber;
    @Range(min = 1, max = 12)
    @AcceptableSymbols(SymbolType.DIGITS)
    private final int expirationMonthDate;
    @Range(min = 21, max = 23)
    @AcceptableSymbols(SymbolType.DIGITS)
    private final int expirationYearDate;
    @AcceptableSymbols(SymbolType.ALPHABETIC)
    private final String firstname;
    @AcceptableSymbols(SymbolType.ALPHABETIC)
    private final String lastname;
    @Fee()
    private Double fee;

    public Request(TransactionType type, int amount, long cardNumber, int expirationMonthDate,
                   int expirationYearDate, String firstname, String lastname) {
        this.type = type;
        this.amount = amount;
        this.cardNumber = cardNumber;
        this.expirationMonthDate = expirationMonthDate;
        this.expirationYearDate = expirationYearDate;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public int getAmount() {
        return amount;
    }

    public int getExpirationMonthDate() {
        return expirationMonthDate;
    }

    public int getExpirationYearDate() {
        return expirationYearDate;
    }
}
