package com.bankingsystem;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.bankingsystem.RequestValidator.*;

public class ProxyBank implements Bank {

    private final Bank bank;

    public ProxyBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void executeTransaction(Request request) {
        try {
            Method getAmount = request.getClass().getDeclaredMethod("getAmount");
            Method getExpirationMonthDate = request.getClass().getDeclaredMethod("getExpirationMonthDate");
            Method getExpirationYearDate = request.getClass().getDeclaredMethod("getExpirationYearDate");

            if (isRequestParamsValid(request, getAmount, getExpirationMonthDate, getExpirationYearDate)) {
                System.out.println("OPERATION SUCCESSFUL");
                bank.executeTransaction(request);
            } else {
                System.out.println("INVALID DATA!");
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            System.out.println("invoke method exception");
        }
    }

    private boolean isRequestParamsValid(Request request, Method getAmount, Method getExpirationMonthDate, Method getExpirationYearDate)
            throws IllegalAccessException, InvocationTargetException {
        return checkLength(request) &&
                checkRange(request, (Integer) getAmount.invoke(request), "amount") &&
                checkRange(
                        request, (Integer) getExpirationMonthDate.invoke(request), "expirationMonthDate") &&
                checkRange(
                        request, (Integer) getExpirationYearDate.invoke(request), "expirationYearDate") &&
                checkSymbolsAcceptability(request, SymbolType.DIGITS, "amount") &&
                checkSymbolsAcceptability(request, SymbolType.DIGITS, "cardNumber") &&
                checkSymbolsAcceptability(request, SymbolType.DIGITS, "expirationMonthDate") &&
                checkSymbolsAcceptability(request, SymbolType.DIGITS, "expirationYearDate") &&
                checkSymbolsAcceptability(request, SymbolType.ALPHABETIC, "firstname") &&
                checkSymbolsAcceptability(request, SymbolType.ALPHABETIC, "lastname");
    }
}
